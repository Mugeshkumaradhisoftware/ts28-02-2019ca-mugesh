package medical;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.*;
import javax.swing.JPanel;
import javax.swing.JTable;

public class BillingOne {

	public BillingOne() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		JFrame f= new JFrame("Medical Shop Management System");  
		JPanel BillingPane=new JPanel(new GridLayout());
		BillingPane.setBounds(27,300,900,300);
        BillingPane.setBackground(Color.WHITE);
        String[][] data = { 
                { "Kundan Kumar Jha", "4031", "CSE" }, 
                { "Anand Jha", "6014", "IT" } 
            }; 
      
            // Column Names 
            String[] columnNames = { "Name", "Roll Number", "Department" }; 
      
            // Initializing the JTable 
            JTable j = new JTable(data, columnNames); 
            j.setBounds(30, 40, 200, 300); 
      
            // adding it to JScrollPane 
            JScrollPane sp = new JScrollPane(j); 
            f.add(sp); 
        
        
        BillingPane.add(j);
   
        f.add(BillingPane);
        f.setSize(1366,768);  
        f.setLayout(null);  
        f.setVisible(true);
	}

}
