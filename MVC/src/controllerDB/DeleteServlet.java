package controllerDB;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.DeleteBean;
 
import dao.DeleteDAO;
 
 
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
     
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String username=request.getParameter("usernamed");
		 
		 System.out.println("username from servlet"+username);
		 DeleteBean delete=new DeleteBean();
		 
		 delete.setUsername(username); 
		 DeleteDAO deletedao= new DeleteDAO(); 
		 deletedao.delete(delete);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> deleted Succesfully </h1>");	           
		 writer.close();
	}

}
