package controllerDB;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.InsertBean;
import bean.UpdateBean;
import dao.InsertDAO;
import dao.UpdateDAO;


public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String username=request.getParameter("usernamett");
		 String password=request.getParameter("passwordtt");
		 
		 System.out.println("username from servlet"+username);
		 UpdateBean update=new UpdateBean();
		 
		 update.setUsername(username); 
		 update.setPassword(password);
		  
		 UpdateDAO updateDao = new UpdateDAO(); 
		 updateDao.update(update);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Update Succesfully </h1>");	           
		 writer.close();
	}

}
