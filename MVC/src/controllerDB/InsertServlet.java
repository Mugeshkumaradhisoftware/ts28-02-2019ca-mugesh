package controllerDB;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.InsertBean;
import bean.LoginBean;
import dao.InsertDAO;
import dao.LoginDAO;
 
public class InsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String username=request.getParameter("usernamet");
		 String password=request.getParameter("passwordt");
		 System.out.println("username from servlet"+username);
		 InsertBean insert=new InsertBean();
		 
		 insert.setUsername(username); 
		 insert.setPassword(password);
		  
		 InsertDAO insertDao = new InsertDAO(); 
		 insertDao.InsertData(insert);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Inserted Succesfully </h1>");	           
		 writer.close();
	  
	}

}
