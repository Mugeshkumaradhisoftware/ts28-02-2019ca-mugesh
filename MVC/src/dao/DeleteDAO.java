package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bean.DeleteBean;
import connection.ConnectionDB;

public class DeleteDAO {
 
	
	public void delete(DeleteBean delete ) {
		String username=delete.getUsername();
		System.out.println("------------->"+username);
		Connection con = null;
	 
	 
		 
		try
		 {
		 con = ConnectionDB.createConnection(); 
		 PreparedStatement stmt=con.prepareStatement("delete from users where username=(?)");
		 stmt.setString(1, username);
		 
		 	
		 int i=stmt.executeUpdate();  
			System.out.println(i+" record deleted");  
			  
			con.close();  
		 }
		 catch(SQLException e)
		 {
		 e.printStackTrace();
		 }
		 
		
		
	}	
}
