package dao;
import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import bean.InsertBean;
import connection.ConnectionDB;
public class InsertDAO {
	public String InsertData(InsertBean insert) {
		String username=insert.getUsername();
		String password=insert.getPassword();
		
		System.out.println("------------->"+username);
		Connection con = null;
	 
	 
		 
		try
		 {
		 con = ConnectionDB.createConnection(); 
		 PreparedStatement stmt=con.prepareStatement("insert into users values(?,?)");
		 stmt.setString(1, username);
		 stmt.setString(2,password);
		 	
		 int i=stmt.executeUpdate();  
			System.out.println(i+" record inserted");  
			  
			con.close();  
		 }
		 catch(SQLException e)
		 {
		 e.printStackTrace();
		 }
		 return "Invalid user credentials"; 
		 
		
		
	}

}
